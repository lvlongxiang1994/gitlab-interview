module gitlab-intervew

go 1.16

require (
	github.com/google/btree v1.0.1
	github.com/stretchr/testify v1.7.1
)
