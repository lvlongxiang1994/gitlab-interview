# RangeList
此 repo 中实现了两种 rangeList `FastPrintRangeList` 及 `RangeList`

`FastPrintRangeList` 用 btree 储存 `range`. `Add(r)` `Remove(r)` 的时间复杂度均为 `O(k*logn)`. k 为跟 r 相交的 range 的数量, n 为 btree 中的 range 数. `Print` 的时间复杂度为 `O(n)`



`RangeList` 用 slice 存储 `range` `Add` `Remove` 的时间复杂度均为 O(1) `Print` 的时间复杂度为 `O(nlogn)`