package range_list

import (
	"fmt"
	"sort"
	"strings"
)

//RangeList Add Remove 的时间复杂度均为 O(1) Print 的时间复杂度为 O(nlogn) 适用于 Add Remove 的调用次数多且传入的 range 大多不相交(如果传入的 range 相交的较多, 可以定期调用 compact 以减少空间), 但 Print 的调用次数少的情况
type RangeList struct {
	added   []*Range
	removed []*Range
}

func (rangeList *RangeList) Add(rangeElement [2]int) error {
	if rangeElement[0] == rangeElement[1] {
		return nil
	}
	rangeList.added = append(
		rangeList.added,
		&Range{From: rangeElement[0], To: rangeElement[1]},
	)
	return nil
}
func (rangeList *RangeList) Remove(rangeElement [2]int) error {
	if rangeElement[0] == rangeElement[1] {
		return nil
	}
	rangeList.removed = append(rangeList.removed, &Range{
		From: rangeElement[0],
		To:   rangeElement[1],
	})
	return nil
}
func (rangeList *RangeList) Print() error {
	fmt.Println(toStr(rangeList.compact()))
	return nil
}

func (rangeList *RangeList) popAddedRange() (*Range, bool) {
	if len(rangeList.added) == 0 {
		return nil, false
	}
	r := rangeList.added[0]

	if len(rangeList.added) == 1 {
		rangeList.added = nil
		return r, true
	}
	rangeList.added = rangeList.added[1:]
	return r, true
}

func (rangeList *RangeList) compact() []*Range {
	rangeList.added = mergeRanges(rangeList.added)

	rangeList.removed = mergeRanges(rangeList.removed)

	result := make([]*Range, 0, len(rangeList.added))
	defer func() {
		rangeList.removed = rangeList.removed[:0]
		rangeList.added = result
	}()

	prev, ok := rangeList.popAddedRange()
	if !ok {
		return nil
	}
	for j := 0; ; {
		if j > len(rangeList.removed)-1 {
			result = append(result, prev)
			result = append(result, rangeList.added...)
			return result
		}
		rmRange := rangeList.removed[j]
		hasIntersection := prev.HasIntersection(rmRange)

		// TODO: 使用 range.Remove
		if hasIntersection {
			if prev.From >= rmRange.From && prev.To <= rmRange.To {
				// prev   :       |______|
				// rmRange:   |_____________|
				if prev, ok = rangeList.popAddedRange(); !ok {
					return result
				}
			} else if prev.From < rmRange.From && prev.To <= rmRange.To {
				//	|___|
				//	  |____|
				result = append(result, NewRange(prev.From, rmRange.From))
				if prev, ok = rangeList.popAddedRange(); !ok {
					return result
				}
			} else if prev.From < rmRange.From && rmRange.To <= prev.To {
				//	|_____________|
				//	   |_____|  |___|
				if prev.From != rmRange.From {
					result = append(result, NewRange(prev.From, rmRange.From))
				}
				prev = NewRange(rmRange.To, prev.To)
				j++
			} else if prev.From < rmRange.To && rmRange.To < prev.To {
				//   |____________|
				// |______|
				prev = NewRange(rmRange.To, prev.To)
				j++
			}
		} else {
			if prev.To < rmRange.From {
				//  |___|
				//   		|__________|
				result = append(result, prev)
				if prev, ok = rangeList.popAddedRange(); !ok {
					return result
				}
			} else {
				// 			|___________|
				// |____|
				j++
			}
		}

	}
}

func toStr(rs []*Range) string {
	sb := strings.Builder{}
	for _, r := range rs {
		sb.WriteString(" " + r.String())
	}
	return sb.String()
}

func mergeRanges(ranges []*Range) []*Range {
	if len(ranges) < 2 {
		return ranges
	}
	sort.Slice(ranges, func(i, j int) bool {
		return ranges[i].From < ranges[j].From
	})
	merged := make([]*Range, 1, len(ranges))
	merged[0] = ranges[0]
	for i := 1; i < len(ranges); i++ {
		lastMerged := merged[len(merged)-1]
		if lastMerged.HasIntersection(ranges[i]) {
			if ranges[i].To > lastMerged.To {
				lastMerged.To = ranges[i].To
			}
		} else {
			merged = append(merged, ranges[i])
		}
	}
	return merged
}
