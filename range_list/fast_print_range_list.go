// Package range_lsit Task: Implement a struct named 'FastPrintRangeList'
// A pair of integers define a range, for example: [1, 5). This range includes integers: 1, 2, 3, and 4.
// A range list is an aggregate of these ranges: [1, 5), [10, 11), [100, 201)
// NOTE: Feel free to add any extra member variables/functions you like.
package range_list

import (
	"fmt"
	"math"

	"github.com/google/btree"
)

// 存的都是*不*相交的 range, 在 insert 时合并相交的 range
type rangeTree struct {
	*btree.BTree
}

func newRangeTree() *rangeTree {
	return &rangeTree{
		BTree: btree.New(10),
	}
}

func (r *Range) Less(than btree.Item) bool {
	return r.From < than.(*Range).From
}

// 如果有相交的, 移除相交的 range, 插入合并后的
func (rt *rangeTree) insert(r *Range) {
	intersectedRanges := rt.popIntersectedRanges(r)
	intersectedRanges = append([]*Range{r}, intersectedRanges...)

	from := math.MaxInt64
	to := math.MinInt64
	intersectedRanges = append(intersectedRanges, r)
	// 这些区间都是相交的, 取最小的 from 及 最大的 to
	for _, ir := range intersectedRanges {
		if ir.From < from {
			from = ir.From
		}
		if ir.To > to {
			to = ir.To
		}
	}

	rt.ReplaceOrInsert(NewRange(from, to))
}

// pop 并返回所有跟 r 相交的 range. 返回的结果按 range.From 升序排列
func (rt *rangeTree) popIntersectedRanges(r *Range) []*Range {
	var intersectedRanges []*Range
	rt.AscendGreaterOrEqual(r, func(i btree.Item) bool {
		if v := i.(*Range); v.HasIntersection(r) {
			intersectedRanges = append(intersectedRanges, v)
			return true
		}
		return false
	})

	var lessThanFrom []*Range
	rt.DescendLessOrEqual(r, func(i btree.Item) bool {
		if v := i.(*Range); v.HasIntersection(r) {
			lessThanFrom = append(lessThanFrom, v)
			return true
		}
		return false
	})

	result := make([]*Range, 0, len(lessThanFrom)+len(intersectedRanges))
	for i := len(lessThanFrom) - 1; i >= 0; i-- {
		result = append(result, lessThanFrom[i])
	}
	result = append(result, intersectedRanges...)

	for _, ir := range result {
		rt.Delete(ir)
	}
	return result
}

func (rt *rangeTree) remove(r *Range) {
	intersectedRanges := rt.popIntersectedRanges(r)
	numIntersected := len(intersectedRanges)
	if numIntersected == 0 {
		return
	}

	first := intersectedRanges[0]
	var remaining []*Range
	remaining = append(remaining, first.Remove(r)...)

	if numIntersected > 1 {
		// 我们不关心中间相交的 Range
		// |__|    |___| |___|
		//  |___________|
		remaining = append(remaining, intersectedRanges[numIntersected-1].Remove(r)...)
	}

	for _, r := range remaining {
		rt.ReplaceOrInsert(r)
	}
}

func (rt *rangeTree) getSortedRanges() []*Range {
	var result []*Range
	rt.Ascend(func(i btree.Item) bool {
		result = append(result, i.(*Range))
		return true
	})
	return result
}

// FastPrintRangeList Add Remove 的时间复杂度均为 O(k*logn)
// k 为跟 r 相交的 range 的数量, n 为 btree 中的 range 数
// Print 的时间复杂度为 O(n)
// 在 Add Remove 时会移除相交的元素. 如果不在意空间, 可以仅 Add/Remove 此时时间复杂度为 O(logn),
type FastPrintRangeList struct {
	*rangeTree
}

func NewRangeList() *FastPrintRangeList {
	return &FastPrintRangeList{
		rangeTree: newRangeTree(),
	}
}

func (rangeList *FastPrintRangeList) Add(rangeElement [2]int) error {
	if rangeElement[0] == rangeElement[1] {
		return nil
	}
	rangeList.insert(NewRange(rangeElement[0], rangeElement[1]))
	return nil
}

func (rangeList *FastPrintRangeList) Remove(rangeElement [2]int) error {
	if rangeElement[0] == rangeElement[1] {
		return nil
	}
	rangeList.remove(NewRange(rangeElement[0], rangeElement[1]))

	return nil
}

func (rangeList *FastPrintRangeList) Print() error {
	fmt.Println(toStr(rangeList.getRanges()))
	return nil
}

func (rangeList *FastPrintRangeList) getRanges() []*Range {
	return rangeList.getSortedRanges()
}
