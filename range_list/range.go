package range_list

import "fmt"

// Range 左闭右开的区间 [From, To)
type Range struct {
	From int
	To   int
}

func NewRange(from int, to int) *Range {
	return &Range{From: from, To: to}
}

func (r *Range) String() string {
	return fmt.Sprintf("[%d, %d)", r.From, r.To)
}

func (r *Range) HasIntersection(r1 *Range) bool {
	if r.To < r1.From || r1.To < r.From {
		return false
	}
	return true
}

func (r *Range) IsValid() bool {
	return r.From < r.To
}
func (r *Range) Remove(rm *Range) []*Range {
	if !r.HasIntersection(rm) {
		return nil
	}

	var result []*Range
	if r.From <= rm.From {
		// r       : |____________|
		// rm case1:   |_____|
		// rm case2:   |_____________|
		if v := NewRange(r.From, rm.From); v.IsValid() {
			result = append(result, v)
		}
		if v := NewRange(rm.To, r.To); v.IsValid() {
			result = append(result, v)
		}
		return result
	} else {
		//	r		:	|_________|
		//  rm case1: |___|
		//  rm case2: |________________|
		if v := NewRange(rm.To, r.To); v.IsValid() {
			return []*Range{v}
		}
	}
	return result
}
