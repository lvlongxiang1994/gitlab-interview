package range_list

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestFastPrintRangeList(t *testing.T) {
	rl := NewRangeList()
	require.NoError(t, rl.Add([2]int{1, 5}))

	require.EqualValues(t, []*Range{
		{
			From: 1,
			To:   5,
		},
	}, rl.getRanges())

	require.NoError(t, rl.Add([2]int{10, 20}))
	require.EqualValues(t, []*Range{
		{
			From: 1,
			To:   5,
		},
		{
			From: 10,
			To:   20,
		},
	}, rl.getRanges(), rl.getRanges())

	require.NoError(t, rl.Add([2]int{20, 20}))
	require.EqualValues(t, []*Range{
		{
			From: 1,
			To:   5,
		},
		{
			From: 10,
			To:   20,
		},
	}, rl.getRanges(), (rl.getRanges()))

	require.NoError(t, rl.Add([2]int{20, 21}))
	// Should display: [1, 5) [10, 21)
	require.EqualValues(t, []*Range{
		{
			From: 1,
			To:   5,
		},
		{
			From: 10,
			To:   21,
		},
	}, rl.getRanges(), (rl.getRanges()))

	require.NoError(t, rl.Add([2]int{2, 4}))
	require.EqualValues(t, []*Range{
		{
			From: 1,
			To:   5,
		},
		{
			From: 10,
			To:   21,
		},
	}, rl.getRanges(), (rl.getRanges()))

	require.NoError(t, rl.Add([2]int{3, 8}))
	require.EqualValues(t, []*Range{
		{
			From: 1,
			To:   8,
		},
		{
			From: 10,
			To:   21,
		},
	}, rl.getRanges(), (rl.getRanges()))

	require.NoError(t, rl.Remove([2]int{10, 10}))
	require.EqualValues(t, []*Range{
		{
			From: 1,
			To:   8,
		},
		{
			From: 10,
			To:   21,
		},
	}, rl.getRanges(), (rl.getRanges()))

	require.NoError(t, rl.Remove([2]int{10, 11}))
	require.EqualValues(t, []*Range{
		{
			From: 1,
			To:   8,
		},
		{
			From: 11,
			To:   21,
		},
	}, rl.getRanges(), (rl.getRanges()))

	require.NoError(t, rl.Remove([2]int{15, 17}))
	// Should display: [1, 8) [11, 15) [17, 21)
	require.EqualValues(t, []*Range{
		{
			From: 1,
			To:   8,
		},
		{
			From: 11,
			To:   15,
		},
		{
			From: 17,
			To:   21,
		},
	}, rl.getRanges(), (rl.getRanges()))

	require.NoError(t, rl.Remove([2]int{3, 19}))
	require.EqualValues(t, []*Range{
		{
			From: 1,
			To:   3,
		},
		{
			From: 19,
			To:   21,
		},
	}, rl.getRanges(), (rl.getRanges()))

	require.NoError(t, rl.Remove([2]int{19, 21}))
	require.EqualValues(t, []*Range{
		{
			From: 1,
			To:   3,
		},
	}, rl.getRanges(), (rl.getRanges()))

}
